const vitaminsItems = require('./vitaminsData');

// 1. Get all items that are available
const availableItems = ((vitaminsItems) => {

    if (vitaminsItems === undefined || !Array.isArray(vitaminsItems)) {
        return [];
    }

    return vitaminsItems.filter((item) => {
        return item.available === true;
    });
});

// 2. Get all items containing only Vitamin C.
const itemsWithOnlyVitaminC = ((vitaminsItems) => {

    if (vitaminsItems === undefined || !Array.isArray(vitaminsItems)) {
        return [];
    }

    return vitaminsItems.filter((item) => {
        return item.contains.trim().toLowerCase() == "vitamin c"
    });
});

// 3. Get all items containing Vitamin A.
const itemsWithVitaminA = ((vitaminsItems) => {
    if (vitaminsItems === undefined || !Array.isArray(vitaminsItems)) {
        return [];
    }

    return vitaminsItems.filter((item) => {
        return item.contains.trim().toLowerCase().includes('vitamin a');
    });
});

// 4. Group items based on the Vitamins they
const groupItemsOnVitamins = ((vitaminsItems) => {
    if (vitaminsItems === undefined || !Array.isArray(vitaminsItems)) {
        return [];
    }

    const itemsGroupedByVitamins = vitaminsItems.reduce((groupedItems, item) => {
        item.contains.split(", ").map((vitaminContain) => {
            groupedItems[vitaminContain] ?? (groupedItems[vitaminContain] = []);
            return groupedItems[vitaminContain].push(item.name);

        });

        return groupedItems;
    }, {});

    return itemsGroupedByVitamins;
});

// 5. 5. Sort items based on number of Vitamins they contain.
const sortItemsOnNumberOfVitamins = ((vitaminsItems) => {
    if (vitaminsItems === undefined || !Array.isArray(vitaminsItems)) {
        return [];
    }

    const sortedItems = vitaminsItems.sort((itemA, itemB) => {
        return itemA.contains.split(",").length > itemB.contains.split(",").length ? 1 : -1;
    });

    return sortedItems;
});




